from django.urls import include, path

from . import views

urlpatterns = [
    path('', views.PersonCreateView.as_view(), name='person_add'),
    path('thanks/', views.success_view, name='success'),
    path('ajax/load-carmodels/', views.load_carmodels, name='ajax_load_carmodels'),
]
