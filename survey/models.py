from django.db import models
# from django.core.validators import RegexValidator
from phonenumber_field.modelfields import PhoneNumberField


class CarBrand(models.Model):
    name = models.CharField(max_length=30, blank=False)

    def __str__(self):
        return self.name


class CarModel(models.Model):
    carbrand = models.ForeignKey(CarBrand, on_delete=models.CASCADE)
    name = models.CharField(max_length=30, blank=False)

    def __str__(self):
        return self.name

class Year(models.Model):
    name = models.CharField(max_length=4)

    def __str__(self):
        return self.name

class Person(models.Model):
    name = models.CharField(max_length=11, blank=False)
    carbrand = models.ForeignKey(CarBrand, on_delete=models.SET_NULL, null=True)
    carmodel = models.ForeignKey(CarModel, on_delete=models.SET_NULL, null=True)
    year = models.ForeignKey(Year, on_delete=models.SET_NULL, null=True )

    def __str__(self):
        return self.name
