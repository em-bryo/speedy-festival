from django.shortcuts import *
from django.template import RequestContext
from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView
from django.urls import reverse_lazy

from . import models
from .models import Person, CarModel
from . import forms
from .forms import PersonForm

class PersonCreateView(CreateView):
    model = Person
    form_class = forms.PersonForm
    success_url = reverse_lazy('success')

def load_carmodels(request):
    carbrand_id = request.GET.get('carbrand')
    carmodels = CarModel.objects.filter(carbrand_id=carbrand_id).order_by('name')
    return render(request, 'survey/carmodel_list.html', {'carmodels': carmodels})

def load_years(self, request):
    years = Year.objects.all()
    return render(request, 'survey/year_list.html', {'years': years})

def success_view(request):
    form = forms.PersonForm()
    if request.method == 'POST':
        form = forms.PersonForm(request.POST)
        if form.is_valid():
            print("Good")
    return render(request, 'survey/success_message.html', {'form': form})