from django import forms
from .models import Person, CarModel, Year
from django.core import validators

class PersonForm(forms.ModelForm):

    class Meta:
        model = Person
        fields = ('name', 'carbrand', 'carmodel', 'year')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['carmodel'].queryset = CarModel.objects.none()

        if 'carbrand' in self.data:
            try:
                carbrand_id = int(self.data.get('carbrand'))
                self.fields['carmodel'].queryset = CarModel.objects.filter(carbrand_id=carbrand_id).order_by('name')
            except (ValueError, TypeError):
                pass  # invalid input from the client; ignore and fallback to empty City queryset
        elif self.instance.pk:
            self.fields['carmodel'].queryset = self.instance.carbrand.carmodel_set.order_by('name')
